package server

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource


/*
    Init MySQL database connection
*/
fun initDB() {
    val config = HikariConfig("/hikari.properties")
    val ds = HikariDataSource(config)
    Database.connect(ds)
//    val url = "jdbc:mysql://jaque:j@localhost:3306/jaque_mate?useUnicode=true&serverTimezone=UTC"
//    val driver = "com.mysql.cj.jdbc.Driver"
//    Database.connect(url, driver)
}

object JaqueParametros : Table("jaque_parametros") {
    val id = integer("id").primaryKey()
    val idContribuyente = (integer("id_ele_contribuyente") references Contribuyentes.id)
    val nombre = varchar("nombre", length = 255)
    val valor = varchar("valor", length = 255)
    val observacion = varchar("observacion", length = 255)
    val estado = varchar("estado", length = 255)
}

object Contribuyentes : Table("ele_contribuyentes") {
    val id = integer("id").primaryKey()
    val documento = varchar("documento", length = 20)
    val razonSocial = varchar("razon_social", length = 500)
    val nombreComercial = varchar("nombre_comercial", length = 500)
    val direccion = varchar("direccion", length = 500)
    val fecha_creacion = datetime("fecha_creacion")
    val fecha_modificacion = datetime("fecha_modificacion")
}

object Documentos : Table("ele_documentos") {
    val id = integer("id").primaryKey()
    val idContribuyente = (integer("id_ele_contribuyente") references Contribuyentes.id)
    val claveAcceso = varchar("clave_acceso", length = 100)
    val establecimiento = varchar("establecimiento", length = 10)
    val puntoEmision = varchar("punto_emision", length = 10)
    val secuencial = varchar("secuencial", length = 20)
    val fechaEmision = datetime("fecha_emision")
    val autorizacion = varchar("autorizacion", length = 100)
    val fechaAutorizacion = varchar("fecha_autorizacion", length = 100)
    val ambiente = varchar("ambiente", length = 50)
    val estado = varchar("estado", length = 50)
    val identificacionReceptor = varchar("identificacionReceptor", length = 20)
    val razonSocialReceptor = varchar("razonSocialReceptor", length = 500)
    val tipo = varchar("tipo", length = 10)
    val propina = decimal("propina", 12, 2)
    val totalDescuentos = decimal("total_descuentos", 12, 2)
    val totalSinImpuestos = decimal("total_sin_impuestos", 12, 2)
    val total = decimal("total", 12, 2)
    val fechaCreacion = datetime("fecha_creacion")
}


data class JaqueParametro(val id: Int,
                          val idContribuyente: Int,
                          val nombre: String,
                          val valor: String,
                          val observacion: String,
                          val estado: String)

enum class Parametro(val nombre: String) {
    XML("XML"),
    PDF("PDF"),
    REPORTES("Reportes"),
    LOGO("Logo"),
    ACTIVO("Activo")
}

enum class TipoComprobante {
    FACTURA,
    RETENCION,
    NOTA_CREDITO,
    NOTA_DEBITO,
    GUIA,
    NO_DEFINIDO
}

fun printParametros() {
    transaction {
        val res = JaqueParametros.selectAll()
        for (f in res) {
            println("${f[JaqueParametros.id]} " +
                    "${f[JaqueParametros.nombre]} " +
                    "${f[JaqueParametros.valor]}"
            )
        }
    }
}

fun getPathParametro(documento : String, parametro : String) : String {
    var valor : String = ""
    println("Parámetro para: $documento")
    transaction {
        (JaqueParametros innerJoin Contribuyentes)
                .slice(Contribuyentes.documento, JaqueParametros.nombre, JaqueParametros.valor)
                .select {
                    Contribuyentes.documento.eq(documento) and
                            JaqueParametros.nombre.eq(parametro) and
                            JaqueParametros.estado.eq(Parametro.ACTIVO.nombre)
                }.forEach {
                    println("${it[JaqueParametros.nombre]} = ${it[JaqueParametros.valor]}")
                    valor = it[JaqueParametros.valor]
                }
    }
    return valor
}

fun getAutorizacion(claveAcceso : String) : String {
    var autorizacion = ""
    transaction {
        Documentos.slice(Documentos.autorizacion)
                .select {
                    Documentos.claveAcceso.eq(claveAcceso)
                }.forEach {
                    autorizacion = it[Documentos.autorizacion]
                }
    }
    return autorizacion
}

fun getFechaAutorizacion(claveAcceso : String) : String {
    var fechaAutorizacion = ""
    transaction {
        Documentos.slice(Documentos.fechaAutorizacion)
                .select {
                    Documentos.claveAcceso.eq(claveAcceso)
                }.forEach {
                    fechaAutorizacion = it[Documentos.fechaAutorizacion]
                }
    }
    return fechaAutorizacion
}
package server

import com.quijotelui.printer.pdf.FacturaPDF
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondFile
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.route
import java.io.File
import java.nio.file.Paths

fun Route.getXml() {
    route("/", HttpMethod.Get) {
        route("xml", HttpMethod.Get) {
            route("/") {
                handle {
                    call.respondText("404", ContentType.Text.Html, HttpStatusCode.NoContent)
                }
            }
            route("{claveAcceso}.xml") {
                handle {
                    val claveAcceso = call.parameters["claveAcceso"]
                    claveAcceso?.let {
                        val xmlPath = getPathParametro(getDocumento(claveAcceso), Parametro.XML.nombre)
                        println("Path completo: $xmlPath" + File.separator + "$claveAcceso.xml")
                        val path = Paths.get("$xmlPath${File.separator}$claveAcceso.xml")
                        val f = path.toFile()
                        println("Existe el archivo XML? ${f.exists()}")
                        if (!f.exists()) call.respondText("404", ContentType.Text.Html, HttpStatusCode.NoContent)
                        else call.respondFile(f)
                    }
                }
            }
        }
    }
}

fun Route.getPdf() {
    route("/", HttpMethod.Get) {
        route("pdf", HttpMethod.Get) {
            route("/") {
                handle {
                    call.respondText("404", ContentType.Text.Html, HttpStatusCode.NoContent)
                }
            }
            route("{claveAcceso}.pdf") {
                handle {
                    val claveAcceso = call.parameters["claveAcceso"]
                    claveAcceso?.let {
                        val pdfPath = getPathParametro(getDocumento(claveAcceso), Parametro.PDF.nombre)
                        println("Path completo: $pdfPath" + File.separator + "$claveAcceso.pdf")
                        val path = Paths.get("$pdfPath${File.separator}$claveAcceso.pdf")
                        val f = path.toFile()
                        println("Existe el archivo PDF? ${f.exists()}")
                        if (!f.exists()){
                            if (createPdf(claveAcceso)) {
                                val pdfFile = Paths.get("$pdfPath${File.separator}$claveAcceso.pdf").toFile()
                                call.respondFile(pdfFile)
                            }
                            else {
                                call.respondText("404", ContentType.Text.Html, HttpStatusCode.NoContent)
                            }
                        }
                        else {
                            call.respondFile(f)
                        }
                    }
                }
            }
        }
    }
}

fun createPdf(claveAcceso : String) : Boolean {
    val xmlPath = getPathParametro(getDocumento(claveAcceso), Parametro.XML.nombre)

    val fileXml = Paths.get("$xmlPath${File.separator}$claveAcceso.xml").toFile()
    println("Existe el archivo XML? ${fileXml.exists()}")
    if (fileXml.exists()) {
        val reportes = getPathParametro(getDocumento(claveAcceso), Parametro.REPORTES.nombre)
        val pdf = getPathParametro(getDocumento(claveAcceso), Parametro.PDF.nombre)
        val logo = getPathParametro(getDocumento(claveAcceso), Parametro.LOGO.nombre)
        val autorizacion = getAutorizacion(claveAcceso)
        val fechaAutorizacion = getFechaAutorizacion(claveAcceso)
        println("Autorización: $autorizacion")
        println("Fecha Autorización: $fechaAutorizacion")

        printPdf(reportes,
                logo,
                pdf,
                "$xmlPath${File.separator}$claveAcceso.xml",
                autorizacion,
                fechaAutorizacion,
                getTipoComprobante(claveAcceso))

        val path = Paths.get("$pdf${File.separator}$claveAcceso.pdf")
        val f = path.toFile()
        if (f.exists()) return true
    }
    return false
}

fun printPdf(pathReportes : String,
             pathLogo :String,
             pathPdf : String,
             pathXml : String,
             autorizacion : String,
             fechaAutorizacion: String,
             tipo : TipoComprobante) {

    if (tipo == TipoComprobante.FACTURA) {
        val pdf = FacturaPDF(pathReportes, pathLogo, pathPdf)
        pdf.genera(pathXml,
                autorizacion,
                fechaAutorizacion)
    }
}
